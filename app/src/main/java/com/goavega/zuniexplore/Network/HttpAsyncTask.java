package com.goavega.zuniexplore.Network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.goavega.zuniexplore.Util.AppConstants;
import com.goavega.zuniexplore.Util.AppUtil;

import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;

/**
 * Created by Administrator on 15-11-2016.
 */
public class HttpAsyncTask extends AsyncTask<String, String, String> {

    private static final String TAG = HttpAsyncTask.class.getSimpleName();

    private Context parentCtx;
    private JSONObject mJsonObject;
    private final ProgressDialog dialog;
    private ServiceListener listener = null;

    private boolean mDisablePD = true ;

    public HttpAsyncTask(Context context, JSONObject jsonObject, ServiceListener serviceListener){

        this.parentCtx = context ;
        this.mJsonObject = jsonObject ;
        this.listener = serviceListener ;
        this.dialog = new ProgressDialog(context);

    }


    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

        if(mDisablePD){
            dialog.setMessage(AppConstants.CONNECTING);
            dialog.setCancelable(false);
            dialog.show();
        }

    }

    @Override
    protected String doInBackground(String... strings) {
        /*if (AppUtil.isInternetAvailable(parentCtx)) {

            byte[] result = null;
            String str = "";
            HttpClient httpclient = new DefaultHttpClient();
            HttpParams myParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(myParams, 10000);
            HttpConnectionParams.setSoTimeout(myParams, 10000);

            JSONObject json = new JSONObject();

            try {

                HttpPost httppost = new HttpPost(params[0]);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");

                System.out.println("url------->"+params[0]);

                json = mJsonObject;

                Log.i(TAG, "+++++++"+json.toString());

                StringEntity se = new StringEntity(json.toString());
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                        "application/json"));
                httppost.setEntity(se);

                HttpResponse response = httpclient.execute(httppost);
                if (response.getEntity().getContentLength()==0) {
                    return AppConstants.SERVER_ERROR;
                }else{
                    result = EntityUtils.toByteArray(response.getEntity());
                    str = new String(result, "UTF-8");
                    Log.i(TAG, "return vaule-->\n"+str);
                    return str;
                }



            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();
                return AppConstants.SERVER_ERROR;

            }	catch (ConnectTimeoutException cte) {
                cte.printStackTrace();
                return AppConstants.SERVER_ERROR;
            }
            catch (SocketTimeoutException ste) {
                ste.printStackTrace();
                return AppConstants.SERVER_ERROR;

            } catch (Exception e) {

                e.printStackTrace();
                return AppConstants.SERVER_ERROR;

            }

        } else {

            return AppConstants.SERVER_ERROR;

        }*/
        return  "mydata";
    }

    @Override
    protected void onPostExecute(String result) {

        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }

        listener.onServiceComplete(result);

    }
}

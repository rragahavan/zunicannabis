package com.goavega.zuniexplore.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.goavega.zuniexplore.Model.Product;
import com.goavega.zuniexplore.R;
import com.goavega.zuniexplore.Util.AppContext;
import com.goavega.zuniexplore.Util.AppUtil;

public class UserDetailsActivity extends AppCompatActivity {

    private Button submitButton;
    private TextView skiptv,restartTv;
    private ImageView restart_Img;
    private AppContext appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_layout);

        restartTv = (TextView)findViewById(R.id.restart_tv);
        restartTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(UserDetailsActivity.this,MainActivity.class);
                startActivity(startIntent);
            }
        });

        skiptv = (TextView)findViewById(R.id.skip_tv);
        skiptv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(UserDetailsActivity.this,Question1.class);
                startActivity(startIntent);
            }
        });

        submitButton = (Button)findViewById(R.id.submit_btn);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(UserDetailsActivity.this,Question1.class);
                startActivity(startIntent);
            }
        });
    }


}
